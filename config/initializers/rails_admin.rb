RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.main_app_name = ["DUL Location Maps", "BackOffice"]
  config.excluded_models = ["Article", "Comment"]

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    #show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
    
  end

  # make Person names display instead of just an ID (defined in Person model)
  config.model 'Collectiongroup' do
    label "Collection Group"
  end

  config.model 'Externallink' do
    label "External Link"
    configure :url do
      label "URL"
    end

    configure :id do
      label "ID"
    end

    configure :genericmessage do
      label "Generic Message"
    end
  end

  config.model 'Collection' do
    label 'Collection'

    list do
      filters [:code, :sublibrary, :collectiongroup]
      fields :code, :label, :sublibrary, :collectiongroup
    end
  end

  config.model 'Genericmessage' do
    label 'Generic Message'

    list do
      field :label
      field :content
    end

    edit do
      field :content, :ck_editor
    end
  end

  config.model 'Sublibrary' do
    configure :locationmap do
      label 'Default Location Map'
    end

    list do
      field :code
      field :label
    end

    show do
      field :code
      field :label
      field :collgroup_choice do
        help "Display (or hide) this Sublibrary in the Collection Group edit form."
      end
      field :locationmap do
        label "Default Location Map"
        help "The default Location Map assigned to this Sublibrary"
      end
    end

    edit do
      field :code
      field :label
      field :collgroup_choice do
        help "Display (or hide) this Sublibrary in the Collection Group edit form."
      end
      field :locationmap do
        label "Default Location Map"
        help "The default Location Map assigned to this Sublibrary"
      end
    end
  end

  config.model 'Locationmap' do
    label 'Location Map'
   
    list do
      field :areamap do
        label 'Area Map'
      end
      field :callno_start do
        label 'Start Call #'
      end
      field :callno_end do
        label 'End Call #'
      end
      field :callno_range do
        label 'Call # Range'
      end
      field :collectiongroup do
        label 'Collection Group'
      end
      field :externallink do
        label 'External Link'
      end
    end

    edit do
      group :callno_fields_group do
        label "Call Number Info"
      end
      callno_fields.each do |f|
        field f[:name] do
          group :callno_fields_group
          label f[:label]
        end
      end
      field :id do
        read_only true
      end
      field :collectiongroup do
        label 'Collection Group'
      end
      field :areamap do
        label 'Area Map'
        help 'Optional. Use this for locations that use a map.'
      end
      field :externallink do
        label 'External Link'
        help 'Optional. Use this for locations that do not have an assigned Area Map.'
      end
    end
  end

  config.model 'Areamap' do
    label 'Area Map'

    edit do
      field :id do
        read_only true
      end
      field :building
      field :area do
        help 'This is normally the floor of the related "Building" (e.g. Floor 1)'
      end
      # this should likely be managed with Active Storage
      field :image
    end
  end

  config.model 'Collectiongroup' do
    label 'Collection Group'

    list do
      field :description
      field :sublibrary
    end
  end

  def callno_fields
    [
      { :name => :callno_start, :label => 'Start' },
      { :name => :callno_end, :label => 'End' },
      { :name => :callno_type, :label => 'Call Number Type'},
      { :name => :callno_range, :label => 'Range' },
      { :name => :callno_priority, :label => 'Display Priority' }
    ]
  end
end
