class AddCollectiongroupToLocationmap < ActiveRecord::Migration[5.1]
  def change
    add_reference :locationmaps, :collectiongroup, foreign_key: false
  end
end
