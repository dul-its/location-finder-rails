class CreateSublibraries < ActiveRecord::Migration[5.1]
  def change
    create_table :sublibraries do |t|
      t.string :code
      t.string :label
      t.boolean :collgroup_choice

      t.timestamps
    end
  end
end
