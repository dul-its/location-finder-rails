class AddExternallinkToLocationmap < ActiveRecord::Migration[5.1]
  def change
    add_reference :locationmaps, :externallink, foreign_key: false
  end
end
