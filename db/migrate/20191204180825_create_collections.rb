class CreateCollections < ActiveRecord::Migration[5.1]
  def change
    create_table :collections do |t|
      t.string :code
      t.string :label

      t.timestamps
    end
  end
end
