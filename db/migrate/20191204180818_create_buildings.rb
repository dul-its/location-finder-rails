class CreateBuildings < ActiveRecord::Migration[5.1]
  def change
    create_table :buildings do |t|
      t.string :name
      t.string :label
      t.string :sublabel
      t.boolean :active
      t.integer :weight
      t.boolean :featured

      t.timestamps
    end
  end
end
