class CreateGenericmessages < ActiveRecord::Migration[5.1]
  def change
    create_table :genericmessages do |t|
      t.string :label
      t.text :content

      t.timestamps
    end
  end
end
