class AdjustLocationmapRefsAgain < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key "locationmaps", "collectiongroups"
    add_foreign_key "locationmaps", "externallinks"
  end
end
