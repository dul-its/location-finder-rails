class CreateCollectiongroups < ActiveRecord::Migration[5.1]
  def change
    create_table :collectiongroups do |t|
      t.text :description
      t.references :sublibrary, foreign_key: true

      t.timestamps
    end
  end
end
