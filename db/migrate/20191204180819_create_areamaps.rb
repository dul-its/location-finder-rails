class CreateAreamaps < ActiveRecord::Migration[5.1]
  def change
    create_table :areamaps do |t|
      t.string :area
      t.string :image
      t.references :building, foreign_key: true

      t.timestamps
    end
  end
end
