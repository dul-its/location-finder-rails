class CreateLocationmaps < ActiveRecord::Migration[5.1]
  def change
    create_table :locationmaps do |t|
      t.string :callno_range
      t.string :callno_type
      t.string :callno_start
      t.string :callno_end
      t.string :map_desc
      t.integer :callno_priority

      t.timestamps
    end
  end
end
