class AddReferencesToCollection < ActiveRecord::Migration[5.1]
  def change
    add_reference :collections, :sublibrary, foreign_key: false
    add_reference :collections, :collectiongroup, foreign_key: false
  end
end
