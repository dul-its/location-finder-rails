class AddLocationmapToSublibrary < ActiveRecord::Migration[5.1]
  def change
    add_reference :sublibraries, :locationmap, foreign_key: false
  end
end
