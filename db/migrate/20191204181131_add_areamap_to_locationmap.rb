class AddAreamapToLocationmap < ActiveRecord::Migration[5.1]
  def change
    add_reference :locationmaps, :areamap, foreign_key: false
  end
end
