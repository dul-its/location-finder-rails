class CreateExternallinks < ActiveRecord::Migration[5.1]
  def change
    create_table :externallinks do |t|
      t.string :url
      t.string :title
      t.string :link_text
      t.text :content

      t.timestamps
    end
  end
end
