class AddGenericmessageToExternallink < ActiveRecord::Migration[5.1]
  def change
    add_reference :externallinks, :genericmessage, foreign_key: false
  end
end
