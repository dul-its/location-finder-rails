# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

seed_file = Rails.root.join('db', 'seeds', 'buildings.yml')
config = YAML::load_file(seed_file)
Building.create!(config)

seed_file = Rails.root.join('db', 'seeds', 'areamaps.yml')
config = YAML::load_file(seed_file)
Areamap.create!(config)

seed_file = Rails.root.join('db', 'seeds', 'sublibraries.yml')
config = YAML::load_file(seed_file)
Sublibrary.create!(config)

seed_file = Rails.root.join('db', 'seeds', 'collectiongroups.yml')
config = YAML::load_file(seed_file)
Collectiongroup.create!(config)

seed_file = Rails.root.join('db', 'seeds', 'collections.yml')
config = YAML::load_file(seed_file)
Collection.create!(config)

seed_file = Rails.root.join('db', 'seeds', 'genericmessages.yml')
config = YAML::load_file(seed_file)
Genericmessage.create!(config)

seed_file = Rails.root.join('db', 'seeds', 'externallinks.yml')
config = YAML::load_file(seed_file)
Externallink.create!(config)

seed_file = Rails.root.join('db', 'seeds', 'locationmaps.yml')
config = YAML::load_file(seed_file)
Locationmap.create!(config)

