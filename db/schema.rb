# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191204214638) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areamaps", force: :cascade do |t|
    t.string "area"
    t.string "image"
    t.bigint "building_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["building_id"], name: "index_areamaps_on_building_id"
  end

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "buildings", force: :cascade do |t|
    t.string "name"
    t.string "label"
    t.string "sublabel"
    t.boolean "active"
    t.integer "weight"
    t.boolean "featured"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "collectiongroups", force: :cascade do |t|
    t.text "description"
    t.bigint "sublibrary_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sublibrary_id"], name: "index_collectiongroups_on_sublibrary_id"
  end

  create_table "collections", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "sublibrary_id"
    t.bigint "collectiongroup_id"
    t.index ["collectiongroup_id"], name: "index_collections_on_collectiongroup_id"
    t.index ["sublibrary_id"], name: "index_collections_on_sublibrary_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "commenter"
    t.text "body"
    t.integer "article_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_comments_on_article_id"
  end

  create_table "externallinks", force: :cascade do |t|
    t.string "url"
    t.string "title"
    t.string "link_text"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "genericmessage_id"
    t.index ["genericmessage_id"], name: "index_externallinks_on_genericmessage_id"
  end

  create_table "genericmessages", force: :cascade do |t|
    t.string "label"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locationmaps", force: :cascade do |t|
    t.string "callno_range"
    t.string "callno_type"
    t.string "callno_start"
    t.string "callno_end"
    t.string "map_desc"
    t.integer "callno_priority"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "areamap_id"
    t.bigint "externallink_id"
    t.bigint "collectiongroup_id"
    t.index ["areamap_id"], name: "index_locationmaps_on_areamap_id"
    t.index ["collectiongroup_id"], name: "index_locationmaps_on_collectiongroup_id"
    t.index ["externallink_id"], name: "index_locationmaps_on_externallink_id"
  end

  create_table "sublibraries", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.boolean "collgroup_choice"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "locationmap_id"
    t.index ["locationmap_id"], name: "index_sublibraries_on_locationmap_id"
  end

  add_foreign_key "areamaps", "buildings"
  add_foreign_key "collectiongroups", "sublibraries"
  add_foreign_key "externallinks", "genericmessages"
  add_foreign_key "locationmaps", "areamaps"
  add_foreign_key "locationmaps", "collectiongroups"
  add_foreign_key "locationmaps", "externallinks"
end
