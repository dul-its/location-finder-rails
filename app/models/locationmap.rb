class Locationmap < ApplicationRecord
  belongs_to :collectiongroup
  belongs_to :areamap, optional: true
  belongs_to :externallink, optional: true
  has_many :sublibraries
end
