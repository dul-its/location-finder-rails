class Building < ApplicationRecord
  has_many :areamaps

  def title
    self.label
  end
end
