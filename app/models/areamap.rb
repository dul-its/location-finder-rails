class Areamap < ApplicationRecord
  belongs_to :building
  
  def title
    t = ""
    begin
      t = "%s: %s" % [ self.building.label, self.area ]
    rescue
      t = ""
    end
    t
  end
end
