class Sublibrary < ApplicationRecord
  belongs_to :locationmap, optional: true

  def title
    t = "%s - %s" % [self.code, self.label]
    t
  end
end
