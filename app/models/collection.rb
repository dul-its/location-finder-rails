class Collection < ApplicationRecord
  belongs_to :sublibrary, optional: true
  belongs_to :collectiongroup, optional: true
end
