class Collectiongroup < ApplicationRecord
  belongs_to :sublibrary

  def name
    self.description
  end

  def title
    t = ""
    begin
      t = "%s: %s" % [self.sublibrary.label, self.description]
    rescue
      t = self.description
    end
    t
    #self.description
  end
end
